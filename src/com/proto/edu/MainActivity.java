package com.proto.edu;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.Window;
import android.widget.FrameLayout;

import com.protoacademy.fragment.CopyOfHomeFragment;
public class MainActivity extends FragmentActivity {
	FragmentManager fragmentManager = null;
	Bundle bundle;
	String FROM = "";
	Fragment fragment = null;
	FrameLayout frameLayout;
	Intent in;
	int ScreenWidth = 0;
	int ScreenHeight = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getActionBar().hide();
		setContentView(R.layout.activity_main);
		frameLayout = (FrameLayout) findViewById(R.id.content_frame);
		String android_id=Secure.getString(MainActivity.this.getContentResolver(),Secure.ANDROID_ID); 
		DisplayMetrics metrics = MainActivity.this.getResources()
				.getDisplayMetrics();
		ScreenWidth = metrics.widthPixels;
		ScreenHeight = metrics.heightPixels;


		fragment = new CopyOfHomeFragment();
		fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		/*
		 * app_logo.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { in = new
		 * Intent(MainActivity.this, LandingPageActivity.class);
		 * startActivity(in); finish(); } });
		 */
	}

}