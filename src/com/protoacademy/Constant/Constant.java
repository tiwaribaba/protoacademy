package com.protoacademy.Constant;

import android.content.Context;
import android.content.Intent;

public class Constant {
	public static String Fragment_name = "";
	public static final String DISPLAY_MESSAGE_ACTION = "com.giggsole.e_wallet.DISPLAY_MESSAGE";

	public static final String EXTRA_MESSAGE = "message";

	public static final String BASE_URL = "http://www.protoacademy.in/api/index.php?path=contactus";

	static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}

}
