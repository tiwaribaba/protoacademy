package com.protoacademy.fragment;

import android.graphics.Paint.Align;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proto.edu.R;
import com.proto.justifiedtext.JustifiedTextView;


public class TabFragment extends Fragment {
	public static final String ARG_SECTION_NUMBER = "section_number";
	private JustifiedTextView JTV1, JTV2;
	public TabFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = null;
		String a = Integer.toString(getArguments().getInt(
				ARG_SECTION_NUMBER));
		if (a.equalsIgnoreCase("1")) {
			rootView = inflater.inflate(R.layout.fragment_tab_e_commerce,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV2 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
			JTV2.setAlignment(Align.LEFT);
		} else if (a.equalsIgnoreCase("2")) {
			rootView = inflater.inflate(R.layout.fragment_tab_web_development,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
		} else if (a.equalsIgnoreCase("3")) {
			rootView = inflater.inflate(R.layout.fragment_tab_mobility,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
		} else if (a.equalsIgnoreCase("4")) {
			rootView = inflater.inflate(R.layout.fragment_tab_testing,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV2 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
			JTV2.setAlignment(Align.LEFT);
		} else if (a.equalsIgnoreCase("5")) {
			rootView = inflater.inflate(R.layout.fragment_tab_seo_sem,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
		} /*else if (a.equalsIgnoreCase("6")) {
			rootView = inflater.inflate(R.layout.fragment_tab_data_analysis,
					container, false);
			rootView = inflater.inflate(R.layout.fragment_tab_testing,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV2 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
			JTV2.setAlignment(Align.LEFT);
		}else if (a.equalsIgnoreCase("7")) {
			rootView = inflater.inflate(R.layout.fragment_tab_behavior_monetisation,
					container, false);
			rootView = inflater.inflate(R.layout.fragment_tab_testing,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV2 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
			JTV2.setAlignment(Align.LEFT);
		}*/ else if (a.equalsIgnoreCase("6")) {
			rootView = inflater.inflate(R.layout.fragment_tab_skill_development,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
		}else {
			rootView = inflater.inflate(R.layout.fragment_tab_e_commerce,
					container, false);
			JTV1 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV2 = (JustifiedTextView) rootView.findViewById(R.id.JTV1);
			JTV1.setAlignment(Align.LEFT);
			JTV2.setAlignment(Align.LEFT);
		}

		
		/*
		 * TextView dummyTextView = (TextView) rootView
		 * .findViewById(R.id.section_label);
		 * dummyTextView.setText(Integer.toString(getArguments().getInt(
		 * ARG_SECTION_NUMBER)));
		 */
		return rootView;
	}
}
