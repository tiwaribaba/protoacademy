package com.protoacademy.fragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proto.edu.R;

public class ContactUsFragment extends Fragment {
	EditText et_Name, et_Email, et_Mobile, et_Comment;
	Button btn_Submit, btn_Cancel;
	private String eMail = "";
	public final static String TAG = ContactUsFragment.class.getSimpleName();

	public ContactUsFragment() {
	}

	public static ContactUsFragment newInstance() {
		return new ContactUsFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_contact_us,
				container, false);
		initView(rootView);
		initData();
		setListner();

		return rootView;
	}

	private void initView(View v) {
		et_Name = (EditText) v.findViewById(R.id.et_Name);
		et_Email = (EditText) v.findViewById(R.id.et_Email);
		et_Mobile = (EditText) v.findViewById(R.id.et_Mobile);
		et_Comment = (EditText) v.findViewById(R.id.et_Comment);
		btn_Submit = (Button) v.findViewById(R.id.btn_Submit);
		btn_Cancel = (Button) v.findViewById(R.id.btn_Cancel);
	}

	private void initData() {
		// conn = new HTTPConnection();
	}

	private void setListner() {
		btn_Submit.setOnClickListener(m_click);
		btn_Cancel.setOnClickListener(m_click);
	}

	private OnClickListener m_click = new OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.btn_Submit:
				InputMethodManager inputManager = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);

				inputManager.hideSoftInputFromWindow(getActivity()
						.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
				eMail = et_Email.getText().toString();

				if (!et_Name.getText().toString().trim().equalsIgnoreCase("")
						&& !et_Email.getText().toString().trim()
								.equalsIgnoreCase("")
						&& !et_Mobile.getText().toString().trim()
								.equalsIgnoreCase("")) {

					if (eMail.matches(emailPattern)) {
						/*
						 * Toast.makeText(getActivity(), "True",
						 * Toast.LENGTH_SHORT).show();
						 */
						new ContactUs(et_Name.getText().toString(), et_Email
								.getText().toString(), et_Mobile.getText()
								.toString(), et_Comment.getText().toString())
								.execute();

					} else {
						Toast.makeText(getActivity(), "Fail",
								Toast.LENGTH_SHORT).show();
					}

				} else {

					Toast.makeText(getActivity(), "Please Enter Valid Fields",
							Toast.LENGTH_SHORT).show();
				}
				break;

			case R.id.btn_Cancel:
				et_Name.setText("");
				et_Email.setText("");
				et_Mobile.setText("");
				et_Comment.setText("");
				break;

			default:
				break;
			}

		}
	};

	public class ContactUs extends AsyncTask<Void, Void, Void> {
		ProgressDialog pd;
		JSONObject jobj;
		String msg = "";
		String Name = "";
		String e_Mail = "";
		String Mobile = "";
		String Query = "";
		String response_string = "";
		HttpResponse settingPOSTresponse;
		String rep = "";
		HttpPost p = new HttpPost(
				"http://www.protoacademy.in/api/index.php?path=contactus");
		HttpClient hc = new DefaultHttpClient();

		public ContactUs(String Name, String e_Mail, String Mobile, String Query) {
			this.Name = Name;
			this.e_Mail = e_Mail;
			this.Mobile = Mobile;
			this.Query = Query;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait..");
			pd.setCancelable(true);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			JSONObject ContactUs = new JSONObject();
			try {
				ContactUs.put("name", Name);
				ContactUs.put("email", e_Mail);
				ContactUs.put("mobile", Mobile);
				ContactUs.put("query", Query);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				p.setEntity(new StringEntity(ContactUs.toString(), "UTF8"));
				p.setHeader("Content-type", "application/json");
				HttpResponse resp = hc.execute(p);
				response_string = EntityUtils.toString(resp.getEntity());
			} catch (Exception e) {
				e.printStackTrace();

			}
			if (response_string != null) {
				try {
					jobj = new JSONObject(response_string);
					rep = jobj.getString("status");

				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pd != null) {
				if (pd.isShowing()) {
					pd.dismiss();

				}
			}

			if (response_string != null) {
				if (rep.equalsIgnoreCase("Success")) {
					Toast.makeText(
							getActivity(),
							"Your query is successfully submitted, We will in touch with You.",
							Toast.LENGTH_SHORT).show();
					et_Name.requestFocus();
					et_Name.setText("");
					et_Email.setText("");
					et_Mobile.setText("");
					et_Comment.setText("");
				} else {
					Toast.makeText(getActivity(), R.string.connection_error,
							Toast.LENGTH_SHORT).show();
					et_Name.requestFocus();
					et_Name.setText("");
					et_Email.setText("");
					et_Mobile.setText("");
					et_Comment.setText("");
				}

			} else {
				Toast.makeText(getActivity(), R.string.connection_error,
						Toast.LENGTH_SHORT).show();
			}

		}
	}

}
