package com.protoacademy.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proto.edu.R;

public class PlacementPartnersFragment extends Fragment {
	
	public final static String TAG = PlacementPartnersFragment.class.getSimpleName();

	public PlacementPartnersFragment() {
	}

	public static PlacementPartnersFragment newInstance() {
		return new PlacementPartnersFragment();
	}

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_placement_partners, container, false);
         
        return rootView;
    }
}
