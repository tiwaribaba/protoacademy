package com.protoacademy.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.proto.edu.R;

public class CopyOfHomeFragment extends Fragment {
	Fragment fragment = null;
	FragmentManager fragmentManager = null;
	ImageView ivHeaderBanner, ivAboutUs, ivCourses, ivPlacePartners, ivAdmission,
			ivFacebook, ivGooglePlus, ivTwitter, ivContactUs;
	LinearLayout llSocialLinking, llLeftMenu, llRightMenu;
	String ProtoAcademy_facebook = "https://www.facebook.com/pages/ProtoAcademy/679776935486089";
	String ProtoAcademy_googleplus = "https://plus.google.com/u/0/113814276365341044594/posts";
	String ProtoAcademy_twitter = "https://twitter.com/protoacademy";
	Intent in;
	int newwidth = 0;
	int socialwidth = 0;
	int ScreenWidth = 0;
	int ScreenHeight = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.copy_of_fragment_home, container,
				false);
		ivHeaderBanner = (ImageView) v.findViewById(R.id.ivHeaderBanner);
		ivAboutUs = (ImageView) v.findViewById(R.id.ivAboutUs);
		ivCourses = (ImageView) v.findViewById(R.id.ivCourses);
		ivPlacePartners = (ImageView) v.findViewById(R.id.ivPlacePartners);
		ivAdmission = (ImageView) v.findViewById(R.id.ivAdmission);
		ivFacebook = (ImageView) v.findViewById(R.id.ivFacebook);
		ivGooglePlus = (ImageView) v.findViewById(R.id.ivGooglePlus);
		ivTwitter = (ImageView) v.findViewById(R.id.ivTwitter);
		ivContactUs = (ImageView) v.findViewById(R.id.ivContactUs);
		llSocialLinking = (LinearLayout) v.findViewById(R.id.llSocialLinking);
		llLeftMenu = (LinearLayout) v.findViewById(R.id.llLeftMenu);
		llRightMenu = (LinearLayout) v.findViewById(R.id.llRightMenu);
		DisplayMetrics metrics = CopyOfHomeFragment.this.getResources()
				.getDisplayMetrics();
		ScreenWidth = metrics.widthPixels;
		ScreenHeight = metrics.heightPixels;

		newwidth = (int) (ScreenWidth / 2.03389);
		
		int menulayoutheight= (int) (ScreenHeight / 1.577);
		//llSocialLinking.getLayoutParams().height = (int) (ScreenHeight / 1.6135);
		llLeftMenu.getLayoutParams().height = menulayoutheight;
		llRightMenu.getLayoutParams().height = menulayoutheight;
		///ivHeaderBanner.getLayoutParams().width = (int) (ScreenWidth / 1.01124);
		//ivHeaderBanner.getLayoutParams().height = (int) (ScreenHeight / 3.42857);
		ivHeaderBanner.getLayoutParams().height = (int) (ScreenHeight / 4.033);

		//ivAboutUs.getLayoutParams().width = newwidth;
		ivAboutUs.getLayoutParams().height = (int)(menulayoutheight-(int) (menulayoutheight / 1.556));

		//ivCourses.getLayoutParams().width = newwidth;
		ivCourses.getLayoutParams().height = (int)(menulayoutheight-(int) (menulayoutheight / 1.429));
		
		//ivPlacePartners.getLayoutParams().width = newwidth;
		ivPlacePartners.getLayoutParams().height = (int)(menulayoutheight-(int) (menulayoutheight / 1.52174));

		//ivAdmission.getLayoutParams().width = newwidth;
		ivAdmission.getLayoutParams().height = (int)(menulayoutheight-(int) (menulayoutheight / 1.751079137));

		//ivContactUs.getLayoutParams().width = (int) (ScreenWidth / 2.03582);
		ivContactUs.getLayoutParams().height = (int)(menulayoutheight-(int) (menulayoutheight / 1.753602305 ));
		socialwidth = (int)(menulayoutheight-(int) (menulayoutheight / 1.164593301));
		//ivFacebook.getLayoutParams().width = socialwidth;
		ivFacebook.getLayoutParams().height = socialwidth;

		//ivTwitter.getLayoutParams().width = socialwidth;
		ivTwitter.getLayoutParams().height = socialwidth;

		//ivGooglePlus.getLayoutParams().width = socialwidth;
		ivGooglePlus.getLayoutParams().height = socialwidth;

		setListner();
		return v;

	}

	private void setListner() {
		ivAboutUs.setOnClickListener(m_click);
		ivCourses.setOnClickListener(m_click);
		ivPlacePartners.setOnClickListener(m_click);
		ivAdmission.setOnClickListener(m_click);
		ivFacebook.setOnClickListener(m_click);
		ivGooglePlus.setOnClickListener(m_click);
		ivTwitter.setOnClickListener(m_click);
		ivContactUs.setOnClickListener(m_click);

	}

	private OnClickListener m_click = new OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.ivAboutUs:
				fragment = new AboutUsFragment();
				fragmentManager = getActivity().getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.addToBackStack("AboutUsFragment")
						.replace(R.id.content_frame, fragment).commit();
				break;
			case R.id.ivCourses:
				fragment = new MenuFragment();
				fragmentManager = getActivity().getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.addToBackStack("MenuFragment")
						.replace(R.id.content_frame, fragment).commit();
				break;
			case R.id.ivPlacePartners:
				fragment = new PlacementPartnersFragment();
				fragmentManager = getActivity().getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.addToBackStack("PlacementPartnersFragment")
						.replace(R.id.content_frame, fragment).commit();
				break;
			case R.id.ivAdmission:
				fragment = new AdmissionFragment();
				fragmentManager = getActivity().getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.addToBackStack("ContactUsFragment")
						.replace(R.id.content_frame, fragment).commit();
				break;

			case R.id.ivFacebook:
				in = new Intent(Intent.ACTION_VIEW);
				in.setData(Uri.parse(ProtoAcademy_facebook));
				startActivity(in);
				break;
			case R.id.ivGooglePlus:
				in = new Intent(Intent.ACTION_VIEW);
				in.setData(Uri.parse(ProtoAcademy_googleplus));
				startActivity(in);
				break;
			case R.id.ivTwitter:
				in = new Intent(Intent.ACTION_VIEW);
				in.setData(Uri.parse(ProtoAcademy_twitter));
				startActivity(in);
				break;
			case R.id.ivContactUs:
				fragment = new ContactUsFragment();
				fragmentManager = getActivity().getSupportFragmentManager();
				fragmentManager.beginTransaction()
						.addToBackStack("ContactUsFragment")
						.replace(R.id.content_frame, fragment).commit();
				break;

			default:
				break;
			}

		}
	};

}
