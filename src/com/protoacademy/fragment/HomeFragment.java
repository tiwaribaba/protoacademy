package com.protoacademy.fragment;

import android.graphics.Bitmap;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.proto.edu.R;
import com.proto.justifiedtext.JustifiedTextView;

public class HomeFragment extends Fragment {
	DisplayImageOptions options;
	Fragment fragment = null;
	private JustifiedTextView JTVHome;

	public final static String TAG = HomeFragment.class.getSimpleName();

	public HomeFragment() {
	}

	public static HomeFragment newInstance() {
		return new HomeFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);
		JTVHome = (JustifiedTextView) rootView.findViewById(R.id.JTVHome);
		JTVHome.setAlignment(Align.LEFT);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		return rootView;
	}
}