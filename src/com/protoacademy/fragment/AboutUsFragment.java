package com.protoacademy.fragment;

import android.graphics.Paint.Align;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proto.edu.R;
import com.proto.justifiedtext.JustifiedTextView;

public class AboutUsFragment extends Fragment {

	public final static String TAG = AboutUsFragment.class.getSimpleName();
	private JustifiedTextView JTVAboutUs;

	public AboutUsFragment() {
	}

	public static AboutUsFragment newInstance() {
		return new AboutUsFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_about_us, container,
				false);
		JTVAboutUs = (JustifiedTextView) rootView.findViewById(R.id.JTVAboutUs);
		JTVAboutUs.setAlignment(Align.LEFT);
		return rootView;
	}
}
