package com.protoacademy.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proto.edu.R;

public class CoursesFragment extends Fragment {
	
	public final static String TAG = CoursesFragment.class.getSimpleName();

	public CoursesFragment() {
	}

	public static CoursesFragment newInstance() {
		return new CoursesFragment();
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_courses, container, false);
         
        return rootView;
    }
}
